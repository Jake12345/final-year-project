﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class P1CharacterAnimations : MonoBehaviour
{

    private Animator animator;
    public float v;
    public float run;
    public float h2Force;
    private bool h1Respawn;
    private bool h2Respawn;
    private bool h3Respawn;

    private bool cooldownOn = false;
  
    // Use this for initialization
    void Start()
    {
        animator = GetComponent<Animator>();
       
    }

    // Update is called once per frame
    void Update()
    {
       
        h1Respawn = H1PlayerMovment.respawning;
        h2Respawn = Hero2PlayerMovment.respawning;
        h3Respawn = Hero3Movment.respawning;

        v = Input.GetAxis("Horizontal");
       
        if (Input.GetKeyDown(KeyCode.Joystick2Button5) || Input.GetKeyDown(KeyCode.Space))
        {
            if (cooldownOn == false)
            {
                animator.SetBool("Jump", true);
                StartCoroutine(dCooldown());
            }
        }

        if (h1Respawn == true || h2Respawn == true || h3Respawn == true)
        {
            Debug.Log("DeathAnimation");
            animator.SetBool("Respawn", true);
        }
   
        if (Input.GetKeyDown(KeyCode.Joystick2Button4))
        {

            animator.SetBool("CrouchWalk ", true);
        }

        Sprinting();

    }


    IEnumerator dCooldown()
    {
        cooldownOn = true;
        yield return new WaitForSeconds(6.95f);
        cooldownOn = false;
    }


    void FixedUpdate()
    {
        animator.SetFloat("Walk", v);
        animator.SetFloat("Run", run);
        //animator.SetFloat("Turn",h);
    }

    void Sprinting()
    {
        //if (Input.GetKey(KeyCode.LeftShift)){
        run = 0.2f;
        //}

    }

}
  