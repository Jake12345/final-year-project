﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class P2CharacterAnimations : MonoBehaviour {


    private Animator animator;
    public float v;
    public float run;
    public float h2Force;
    private bool h1Respawn;
    private bool h2Respawn;
    private bool h3Respawn;

    private bool cooldownOn = false;


    // Use this for initialization
    void Start()
    {
        
        
        animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        h1Respawn = P2H1Movement.respawning;
        h2Respawn = P2H2Movement.respawning;
        h3Respawn = Hero3P2PMovement.respawning;

        v = Input.GetAxis("HorizontalP2");
       
        if (Input.GetKeyDown(KeyCode.Joystick1Button5) || Input.GetKeyDown(KeyCode.Keypad0))
        {
            if (cooldownOn == false)
            {
                animator.SetBool("Jump", true);
                StartCoroutine(dCooldown());
            }              
        }


        if (h1Respawn == true || h2Respawn == true || h3Respawn == true)
        {
            animator.SetBool("Respawn", true);
            Debug.Log("DeathAnimation");
        }
  

        if (Input.GetKeyDown(KeyCode.Joystick1Button4))
        {

            animator.SetBool("CrouchWalk ", true);
        }

        Sprinting();

    }

    IEnumerator dCooldown()
    {
        cooldownOn = true;
        yield return new WaitForSeconds(6.95f);
        cooldownOn = false;
    }

    void FixedUpdate()
    {
        animator.SetFloat("Walk", v);
        animator.SetFloat("Run", run);
        //animator.SetFloat("Turn",h);
    }

    void Sprinting()
    {
        //if (Input.GetKey(KeyCode.LeftShift)){
        run = 0.2f;
        //}

    }

}
