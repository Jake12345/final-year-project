﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Music : MonoBehaviour {

    // Use this for initialization

    [SerializeField]
    private AudioClip[] audioClips;

    private bool playing = false;
    private bool leftMenu = false;
    private string sceneName;

    static bool AudioBegin = false;

    void Awake()
    {       
        sceneName = "blank";
        PlaySound(0);
        DontDestroyOnLoad(gameObject);
    }
    void Update()
    {
        //Debug.Log(Application.loadedLevelName);
        //Debug.Log(gameLoop);


        if (Application.loadedLevelName != sceneName)
        {
          

            if (Application.loadedLevelName == "MenuScene" && leftMenu == true)
            {
                Debug.Log("Music Script destoryed");
                Destroy(this.gameObject);
            }

            if (Application.loadedLevelName != "MenuScene")
            {
                leftMenu = true;
            }

            if (Application.loadedLevelName == "Map2" || Application.loadedLevelName == "SceneDemo")
            {
                PlaySound(1);                
            }

            if (Application.loadedLevelName == "WinLoseScene")
            {
                PlaySound(2);
            }

            sceneName = Application.loadedLevelName;  
        }

    }


    void PlaySound(int clip)
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.clip = audioClips[clip];
        audio.Play();
    }

}
