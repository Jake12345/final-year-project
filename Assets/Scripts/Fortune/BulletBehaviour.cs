﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletBehaviour : MonoBehaviour {


    [SerializeField]
    private Transform explosionPrefab;

    [SerializeField]
    private float life;


    // Use this for initialization
    void Start ()
    {
        StartCoroutine(delay());
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnCollisionEnter(Collision collision)
    {

        Quaternion rot = this.transform.rotation;
        Vector3 pos = this.transform.position;
        Instantiate(explosionPrefab, pos, rot);
        Destroy(this.gameObject);

    }
    IEnumerator delay() //after a 02 second delay the mine wil spawn at the location the ball was at 0.2 seconnds 
    {
        yield return new WaitForSeconds(life);
        Destroy(this.gameObject);
    }

}
