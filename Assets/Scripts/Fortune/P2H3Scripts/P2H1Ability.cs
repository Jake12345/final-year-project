﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class P2H1Ability : MonoBehaviour {


    public float maxHealth;
    public float health;
    private GameObject healthbar;
    Collision collision;

    public static bool gagefull = false;
    private GameObject gageFullBar;
    private bool gageFullSoundPlayed = false;

    [SerializeField]
    private AudioSource audioBullets;

    [SerializeField]
    private GameObject Bullet;
    [SerializeField]
    private GameObject leftSpawn;
    [SerializeField]
    private GameObject middleSpawn;
    [SerializeField]
    private GameObject rightSpawn;
    [SerializeField]
    private GameObject middleRightSpawn;
    [SerializeField]
    private GameObject middleLeftSpawn;


    public static bool fortuneFiring = false;

    private bool startIntro = true;

    // Use this for initialization
    void Start()
    {
        healthbar = GameObject.Find("P2HealthBar ");
        gageFullBar = GameObject.Find("P2GageFull");

        health = maxHealth;
        gageFullBar.SetActive(false);

    }

    // Update is called once per frame
    void Update()
    {

        startIntro = StartCountdown.startIntro;

        if (health > 0)
        {
            DecreaseHealth();
        }

        if (health == 0)
        {
            if (gageFullSoundPlayed == false)
            {
                this.GetComponent<AudioSource>().Play();
                gageFullSoundPlayed = true;
            }
            gagefull = true;
            gageFullBar.SetActive(true);
        }

        if (startIntro == false)
        {          
                if (Input.GetKeyUp(KeyCode.Keypad9) && gagefull == true || Input.GetKeyDown(KeyCode.Joystick1Button4) && gagefull == true)
                {
                    health = maxHealth;
                    gageFullBar.SetActive(false);
                    gageFullSoundPlayed = false;
                    audioBullets.Play();
                    StartCoroutine(bulletfiring());
                    Debug.Log("P2H1Firing");
                }          
        }
    }

    IEnumerator bulletfiring() //after a 02 second delay the mine wil spawn at the location the ball was at 0.2 seconnds 
    {
        fortuneFiring = true;
        yield return new WaitForSeconds(4.6f);
        shoot();
        yield return new WaitForSeconds(0.3f);
        shoot();
        yield return new WaitForSeconds(0.3f);
        shoot();
        fortuneFiring = false;
        gagefull = false;
    }

    void shoot()
    {
        GameObject g = Instantiate(Bullet);
        GameObject h = Instantiate(Bullet);
        GameObject i = Instantiate(Bullet);
        GameObject j = Instantiate(Bullet);
        GameObject k = Instantiate(Bullet);

        g.transform.position = middleSpawn.transform.position;
        g.transform.rotation = middleSpawn.transform.rotation;

        h.transform.position = leftSpawn.transform.position;
        h.transform.rotation = leftSpawn.transform.rotation;

        i.transform.position = rightSpawn.transform.position;
        i.transform.rotation = rightSpawn.transform.rotation;

        j.transform.position = middleLeftSpawn.transform.position;
        j.transform.rotation = middleLeftSpawn.transform.rotation;

        k.transform.position = middleRightSpawn.transform.position;
        k.transform.rotation = middleRightSpawn.transform.rotation;
    }

    void DecreaseHealth()
    {
        health = health - 1;
        float currentHealth = health / maxHealth;
        SetHealthBar(currentHealth);

    }

    public void SetHealthBar(float myHealth)
    {
        healthbar.transform.localScale = new Vector3(Mathf.Clamp(myHealth, 0f, 1f), healthbar.transform.localScale.y, healthbar.transform.localScale.z);//This is what lowers the health bar
    }
}
