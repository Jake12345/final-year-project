﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class P1CameraBehivour : MonoBehaviour {

    public GameObject target;//the target object
    private float speedMod = 10.0f;//a speed modifier
    private Vector3 point;//the coord to the point where the camera looks at
    private bool startIntro = true;
    [SerializeField]
    private Camera thisCamera;
    [SerializeField]
    private int heroNumber;

    private bool reverseMove;

    private float direction = 0.0f;

    void Start()
    {//Set up things on the start method
        point = target.transform.position;//get target's coords
        transform.LookAt(point);//makes the camera look to it

        transform.position = new Vector3(transform.position.x, 877, transform.position.z);
    }

    void Update()
    {
        startIntro = StartCountdown.startIntro;

        if(heroNumber == 1)
        {
            reverseMove = H1PlayerMovment.reverseMove;
        }
        if (heroNumber == 2)
        {
            reverseMove = Hero2PlayerMovment.reverseMove;
        }
        if (heroNumber == 3)
        {
            reverseMove = Hero3Movment.reverseMove;
        }


        direction = Input.GetAxis("Vertical");

        if (reverseMove == true) // this one needs to reverse the direction 
        {
            direction *= -1;
            transform.RotateAround(point, new Vector3(0.0f, direction, 0.0f), 20 * Time.deltaTime * speedMod);
        }
        else
        {
            transform.RotateAround(point, new Vector3(0.0f, direction, 0.0f), 20 * Time.deltaTime * speedMod);
        }

        if (startIntro == false)
        {
            if (Input.GetKey(KeyCode.A))
            {
                if (reverseMove == true)
                {
                    transform.RotateAround(point, new Vector3(0.0f, 1.0f, 0.0f), 20 * Time.deltaTime * speedMod);
                }
                else
                {
                    transform.RotateAround(point, new Vector3(0.0f, -1.0f, 0.0f), 20 * Time.deltaTime * speedMod);
                }
            }
            if (Input.GetKey(KeyCode.D))
            {
                if (reverseMove == true)
                {
                    transform.RotateAround(point, new Vector3(0.0f, -1.0f, 0.0f), 20 * Time.deltaTime * speedMod);
                }
                else
                {
                    transform.RotateAround(point, new Vector3(0.0f, 1.0f, 0.0f), 20 * Time.deltaTime * speedMod);
                }
            }
        }
    }
}
