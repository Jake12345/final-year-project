﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MineBehavior : MonoBehaviour
{



    [SerializeField]
    private Transform explosionPrefab;

    [SerializeField]
    private float life;

    public AudioClip[] audioClips;

    // Use this for initialization
    void Start()
    {
        StartCoroutine(timer());
    }

    // Update is called once per frame
    void Update()
    {

    }


    void OnTriggerEnter(Collider other)
    {

   

    }

    void OnCollisionEnter(Collision collision)
    {
        //Debug.Log("respawning");

        if (collision.gameObject.layer == 12)
        {
            Quaternion rot = this.transform.rotation;
            Vector3 pos = this.transform.position;
            Instantiate(explosionPrefab, pos, rot);
            PlaySound(0);
            StartCoroutine(delay());
        }


     
    }


    void PlaySound(int clip)
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.clip = audioClips[clip];
        audio.Play();
    }

    IEnumerator timer() //after a 02 second delay the mine wil spawn at the location the ball was at 0.2 seconnds 
    {
        yield return new WaitForSeconds(life);
        this.gameObject.SetActive(false);
    }
    IEnumerator delay() //after a 02 second delay the mine wil spawn at the location the ball was at 0.2 seconnds 
    {
        yield return new WaitForSeconds(2);
        this.gameObject.SetActive(false);
    }

}
