﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class P2H2Ability : MonoBehaviour {



    public float maxHealth;
    public float health;
    private GameObject healthbar;
    Collision collision;

    public static bool gagefull = false;
    private GameObject gageFullBar;
    private bool gageFullSoundPlayed = false;

    [SerializeField]
    public GameObject mineSpawnPoint;
    private Vector3 delaySpawn;

    [SerializeField]
    private List<GameObject> pooledObjects;
    [SerializeField]
    private GameObject objectToPool;
    [SerializeField]
    private int amountToPool;

    private bool startIntro = true;

    // Use this for initialization
    void Start()
    {

        healthbar = GameObject.Find("P2HealthBar ");
        gageFullBar = GameObject.Find("P2GageFull");

        health = maxHealth;
        gageFullBar.SetActive(false);

        pooledObjects = new List<GameObject>();
        for (int i = 0; i < amountToPool; i++)
        {
            GameObject obj = (GameObject)Instantiate(objectToPool);
            obj.SetActive(false);
            pooledObjects.Add(obj);
        }       
    }

    // Update is called once per frame
    void Update()
    {

        startIntro = StartCountdown.startIntro;

        if (health > 0)
        {
            DecreaseHealth();
        }

        if (health == 0)
        {
            if (gageFullSoundPlayed == false)
            {
                this.GetComponent<AudioSource>().Play();
                gageFullSoundPlayed = true;
            }
            gagefull = true;
            gageFullBar.SetActive(true);
        }

        if (startIntro == false)
        {
                if (Input.GetKeyUp(KeyCode.Keypad9) && gagefull == true || Input.GetKeyDown(KeyCode.Joystick1Button4) && gagefull == true)
                {
                    health = maxHealth;
                    gageFullBar.SetActive(false);
                    gageFullSoundPlayed = false;
                    Debug.Log("player 2 placed a mine");
                    StartCoroutine(Example());
                }           
        }
    }

    IEnumerator Example() //after a 02 second delay the mine wil spawn at the location the ball was at 0.2 seconnds 
    {
        GameObject temp;

        delaySpawn = new Vector3(mineSpawnPoint.transform.position.x, mineSpawnPoint.transform.position.y + 1.0f, mineSpawnPoint.transform.position.z);
        yield return new WaitForSeconds(0.3f);

        for (int i = 0; i < pooledObjects.Count; i++)
        {
            if (!pooledObjects[i].activeInHierarchy)
            {
                temp = pooledObjects[i];
                temp.SetActive(true);
                temp.transform.position = delaySpawn;
                break;
            }
        }
        gagefull = false;
    }


    void DecreaseHealth()
    {
        health = health - 1;
        float currentHealth = health / maxHealth;
        SetHealthBar(currentHealth);
    }

    public void SetHealthBar(float myHealth)
    {
        healthbar.transform.localScale = new Vector3(Mathf.Clamp(myHealth, 0f, 1f), healthbar.transform.localScale.y, healthbar.transform.localScale.z);//This is what lowers the health bar
    }
}
