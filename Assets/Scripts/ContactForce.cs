﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Effects;

public class ContactForce : MonoBehaviour {


    public float explosionForce = 4;
    [SerializeField]
    private Transform sparksPrefab;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private IEnumerator OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.layer == 12)
        {
            float multiplier = GetComponent<ParticleSystemMultiplier>().multiplier;


            Quaternion rot = this.transform.rotation;
            Vector3 pos = this.transform.position;
            Instantiate(sparksPrefab, pos, rot);



            float r = 10 * multiplier;
            var cols = Physics.OverlapSphere(transform.position, r);
            var rigidbodies = new List<Rigidbody>();
            foreach (var col in cols)
            {
                if (col.attachedRigidbody != null && !rigidbodies.Contains(col.attachedRigidbody))
                {
                    rigidbodies.Add(col.attachedRigidbody);
                }
            }
            foreach (var rb in rigidbodies)
            {
                rb.AddExplosionForce(explosionForce * multiplier, transform.position, r, 1 * multiplier, ForceMode.Impulse);
            }

            yield return new WaitForSeconds(3);
            
        }

    }
    

}
