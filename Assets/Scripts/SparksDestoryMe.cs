﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SparksDestoryMe : MonoBehaviour {

	// Use this for initialization
	void Start ()
    {
        StartCoroutine(Destory());
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    
    IEnumerator Destory()
    {
      
        yield return new WaitForSeconds(1);
        Destroy(this.gameObject);
    }

}
