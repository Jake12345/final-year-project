﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArrowsControls : MonoBehaviour {

    public Camera cameraq;
    public float acceleration;
    private Rigidbody rb;
    private AudioSource rollSound;

    private bool spacePressed = false;
    public static bool gagefull = false;

    // Use this for initialization
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        rollSound = GetComponent<AudioSource>();
    }



    void Update()
    {

        gagefull = AbilityGage.gagefull;
        Debug.Log(gagefull);

        if (Input.GetKey("up"))
        {
            rb.AddForce(acceleration * cameraq.transform.forward * Time.deltaTime);

        }

        if (Input.GetKey("down"))
        {
            rb.AddForce(acceleration * -cameraq.transform.forward * Time.deltaTime);
        }

        if (Input.GetKey("q"))
        {
            rb.AddForce(acceleration * -cameraq.transform.right * Time.deltaTime);
        }

        if (Input.GetKey("e"))
        {
            rb.AddForce(acceleration * cameraq.transform.right * Time.deltaTime);
        }


        if (Input.GetKeyUp(KeyCode.Space) && gagefull == true)
        {
            rb.AddForce(cameraq.transform.forward * 1000);
            AbilityGage.gagefull = false;
            Debug.Log("space pressed");
        }
    }

}
