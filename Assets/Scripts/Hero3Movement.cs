﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hero3Movement : MonoBehaviour {

    public Camera cameraq;
    public float acceleration = 1000;
    private Rigidbody rb;
    private AudioSource rollSound;

    private bool spacePressed = false;
    public static bool gagefull = false;

    // Use this for initialization
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        rollSound = GetComponent<AudioSource>();
    }



    void Update()
    {

        gagefull = Hero3AbilityGage.gagefull;
        //Debug.Log(gagefull);

        if (Input.GetKey("w"))
        {
            rb.AddForce(acceleration * cameraq.transform.forward * Time.deltaTime);

        }

        if (Input.GetKey("s"))
        {
            rb.AddForce(acceleration * -cameraq.transform.forward * Time.deltaTime);
        }


        if (Input.GetKeyUp(KeyCode.Space) && gagefull == true)
        {

            Hero2AbilityGage.gagefull = false;
            Debug.Log("space pressed");
        }





    }


}
