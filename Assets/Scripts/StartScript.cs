﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartScript : MonoBehaviour {


    [SerializeField]
    private GameObject p1BigBoy;
    [SerializeField]
    private GameObject p1Miner;
    [SerializeField]
    private GameObject p1Fortune;
    [SerializeField]
    private GameObject player2BigBoy;
    [SerializeField]
    private GameObject player2Miner;
    [SerializeField]
    private GameObject player2Fortune;
    [SerializeField]
    private GameObject player1Spawn;
    [SerializeField]
    private GameObject player2Spawn;

    private int player1Hero;
    private int player2Hero;
    private int lives;

    // Use this for initialization
    void Start ()
    {
        player1Hero = CharacterSelect.player1Choice;
        player2Hero = CharacterSelect.player2Choice;
      
        //player1Hero = 1;
        //player2Hero = 2;

        Debug.Log("player1Hero = " + player1Hero);
        Debug.Log("player2Hero = " + player2Hero);

   


        switch (player1Hero)
        {
            case 0:
                Instantiate(p1BigBoy, player1Spawn.transform.position, player1Spawn.transform.rotation);

                Debug.Log("Player 1 = BigBOy");
                break;
            case 1:
                Instantiate(p1Miner, player1Spawn.transform.position, player1Spawn.transform.rotation);

                Debug.Log("Player 1 = Miner");
                break;
            case 2:
                Instantiate(p1Fortune, player1Spawn.transform.position, player1Spawn.transform.rotation);
                Debug.Log("Player 1 = Fortune");
                break;
        }

        switch (player2Hero)
        {
            case 0:
                Instantiate(player2BigBoy, player2Spawn.transform.position, player2Spawn.transform.rotation);
                Debug.Log("Player 2 = Big Boy");
                break;
            case 1:
                Instantiate(player2Miner, player2Spawn.transform.position, player2Spawn.transform.rotation);
                Debug.Log("Player 2 = Miner");
                break;
            case 2:
                Instantiate(player2Fortune, player2Spawn.transform.position, player2Spawn.transform.rotation);
                Debug.Log("Player 2 = Fortune");
                break;
        }
    }
}
