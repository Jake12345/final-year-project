﻿using UnityEngine;
using System.Collections;

public class AbilityGage : MonoBehaviour
{


    public float maxHealth;
    public float health;
    public GameObject healthbar;
    Collision collision;
    public static bool gagefull = false;

    public GameObject gageFullBar;
    private bool gageFullSoundPlayed = false;


	// Use this for initialization
	void Start () 
    {
        health = maxHealth;
        gageFullBar.SetActive(false);

    }
	
	// Update is called once per frame
	void Update ()
    {

        if (Input.GetKeyUp("space") && gagefull == true)
        {
            health = maxHealth;
            gageFullBar.SetActive(false);
            gageFullSoundPlayed = false;
        }

        if (health > 0)
        {
            DecreaseHealth();
           // Debug.Log(health);
        }

        if (health == 0)
        {
           
            if (gageFullSoundPlayed == false)
            {
                this.GetComponent<AudioSource>().Play();
                gageFullSoundPlayed = true;
            }

            gagefull = true;
            gageFullBar.SetActive(true);


        }

    }

    void DecreaseHealth()
    {
        health = health - 1;
        float currentHealth = health / maxHealth;
        SetHealthBar(currentHealth);
      
    }

   
    public void SetHealthBar(float myHealth)
    {
        healthbar.transform.localScale = new Vector3(Mathf.Clamp(myHealth, 0f, 1f), healthbar.transform.localScale.y, healthbar.transform.localScale.z);//This is what lowers the health bar
      
    }

    void OnTriggerEnter(Collider other)
    {
        //if (shieldOn == false)
        //{
            //Debug.Log("Bullet hit Boat");
            //DecreaseHealth();
        //}
    }

    void OnCollisionEnter(Collision collision)
    {
        //if (collision.gameObject.tag == "Torpedo" && shieldOn == false) //will only take damage if the shield is offline
        //{
           // Debug.Log("Torpedo hit Boat");
            //DecreaseHealth2();
        //}
    }
}
