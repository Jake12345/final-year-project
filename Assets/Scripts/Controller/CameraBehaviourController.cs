﻿using UnityEngine;
using System.Collections;

public class CameraBehaviourController : MonoBehaviour {

    public GameObject target;//the target object
    private float speedMod = 10.0f;//a speed modifier
    private Vector3 point;//the coord to the point where the camera looks at
    private Camera normalCam;


    void Start()
    {   //Set up things on the start method
        point = target.transform.position;//get target's coords
        transform.LookAt(point);//makes the camera look to it


        


    }

    void Update()
    {       
        //makes the camera rotate around "point" coords, rotating around its Y axis, 20 degrees per second times the speed modifier
            transform.RotateAround(point, new Vector3(0.0f, Input.GetAxis("HorizontalXbox"), 0.0f), 20 * Time.deltaTime * speedMod);
    }


    void OnTriggerEnter(Collider powerUp)
    {
        if (powerUp.gameObject.tag == "Wall")
        {
            Debug.Log("Camera Hits Wall");
        }
    }



    //NOT BEING USED!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
   
}
