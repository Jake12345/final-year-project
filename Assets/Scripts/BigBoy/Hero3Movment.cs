﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Hero3Movment : MonoBehaviour
{

    public Camera cameraq;
    public float acceleration;
    private Rigidbody rb;
    private AudioSource rollSound;

    private bool spacePressed = false;
    public static bool gagefull = false;
   
    private GameObject spawnLocation;

    [SerializeField]
    private float PowerReverseTime;

    private bool startIntro = true;

    private Text livesText;

    private string player;

    public static int h3Lives;

    public static Vector3 ballLocation;

    public static bool reverseMove = false;

    private bool h2lightNightDisable = false;
    public static bool powerUpLightningCountdown = false;
 
    [SerializeField]
    private AudioSource audioLightnigh;

    private bool PowerUpPush;
    [SerializeField]
    private GameObject confuseMarks;
    [SerializeField]
    private GameObject fireTrail;
    [SerializeField]
    private GameObject H2Lightning;
    [SerializeField]
    private GameObject pushExclamationMrk;


    float x = 0;
    private float buffer = 1.5f;

    private bool grow = false;
    private bool shrink;

    private bool p1H1powerUpLightningCountdown = false;
    private bool p1H2powerUpLightningCountdown = false;
    
    private bool p2H1powerUpLightningCountdown = false;
    private bool p2H2powerUpLightningCountdown = false;
    private bool p2H3powerUpLightningCountdown = false;

    public static bool respawning = false;

    private bool dashCooldown = false;

    [SerializeField]
    private int dashCooldownTimer;

    // Use this for initialization
    void Start()
    {
     
        livesText = GameObject.Find("P1Lives").GetComponent<Text>();
        h3Lives = GameSelect.lives;
        //h3Lives = 4;
        rb = GetComponent<Rigidbody>();
        rollSound = GetComponent<AudioSource>();
        livesText.text = "Lives: " + h3Lives.ToString();

        spawnLocation = GameObject.Find("Player1Spawn");
       
    }

    void Update()
    {
        respawning = false;

        ballLocation = new Vector3(transform.position.x, transform.position.y + buffer, transform.position.z);

        p1H1powerUpLightningCountdown = H1PlayerMovment.powerUpLightningCountdown;
        p1H2powerUpLightningCountdown = Hero2PlayerMovment.powerUpLightningCountdown;       
        p2H1powerUpLightningCountdown = P2H1Movement.powerUpLightningCountdown;
        p2H2powerUpLightningCountdown = P2H2Movement.powerUpLightningCountdown;
        p2H3powerUpLightningCountdown = Hero3P2PMovement.powerUpLightningCountdown;

        gagefull = AbilityGage.gagefull;
        startIntro = StartCountdown.startIntro;

        grow = Hero3AbilityGage.grow;
        shrink = Hero3AbilityGage.shrink;

        x = Input.GetAxis("Horizontal");

        if (grow == true)
        {
            buffer = buffer + 0.25f * Time.deltaTime;
        }

        if (shrink == true)
        {
            buffer = buffer - 0.25f * Time.deltaTime;
        }

        if (startIntro == false)
        {
            if (h2lightNightDisable == false)
            {
                if (x > 0 || (Input.GetKey(KeyCode.W)))
                {
                    if (reverseMove == true)
                    {
                        rb.AddForce(acceleration * -cameraq.transform.forward * Time.deltaTime);
                        rb.AddForce(-acceleration * cameraq.transform.up * Time.deltaTime);
                    }
                    else
                    {
                        rb.AddForce(acceleration * cameraq.transform.forward * Time.deltaTime);
                    }
                }
                if (x < 0  || (Input.GetKey(KeyCode.S)))
                {
                    if (reverseMove == true)
                    {
                        rb.AddForce(acceleration * cameraq.transform.forward * Time.deltaTime);

                    }
                    else
                    {
                        rb.AddForce(acceleration * -cameraq.transform.forward * Time.deltaTime);
                        rb.AddForce(-acceleration * cameraq.transform.up * Time.deltaTime);
                    }
                }

                if (Input.GetKeyUp(KeyCode.Space) && dashCooldown == false || Input.GetKeyDown(KeyCode.Joystick2Button5) && dashCooldown == false)
                {
                    rb.AddForce(cameraq.transform.forward * 4500);
                    StartCoroutine(dCooldown());
                }
            }
        }

        if (PowerUpPush == true)
        {
            rb.AddForce(10000 * cameraq.transform.forward * Time.deltaTime);          
        }

        if (Input.GetKeyUp(KeyCode.Space) && gagefull == true)
        {
            rb.AddForce(cameraq.transform.forward * 1000);
            AbilityGage.gagefull = false;
            Debug.Log("space pressed");
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "PowerUp")
        {
            int randomNumber = UnityEngine.Random.Range(0, 3);
            //int randomNumber = 0;

            Debug.Log(randomNumber);

            if (randomNumber == 0)
            {
                StartCoroutine(Reverse());
            }
            if (randomNumber == 1)
            {
                StartCoroutine(Push());
            }
            if (randomNumber == 2)
            {
                StartCoroutine(Lightning());
            }
        }
    }

    void OnCollisionEnter(Collision collision)
    {

        if (collision.gameObject.tag == "Floor") //will only take damage if the shield is offline
        {
            respawning = true;
            rb.velocity = Vector3.zero;

            h3Lives--;
            if (1 > h3Lives)
            {
                SceneManager.LoadScene("WinLoseScene");
            }
            Debug.Log("respawning");
            this.transform.position = spawnLocation.transform.position;
            cameraq.transform.position = spawnLocation.transform.position;

            livesText.text = "Lives: " + h3Lives.ToString();
        }

        if (p1H1powerUpLightningCountdown == true || p1H2powerUpLightningCountdown == true || p2H1powerUpLightningCountdown == true || p2H2powerUpLightningCountdown == true || p2H3powerUpLightningCountdown == true)
        {
            StartCoroutine(Lightning2());
        }

        if (collision.gameObject.tag == "Fortune")
        {
            if (powerUpLightningCountdown == true || p1H1powerUpLightningCountdown == true)
            {
                if (H2Lightning.activeInHierarchy == true)
                {
                    H2Lightning.SetActive(false);
                }
                else
                    H2Lightning.SetActive(true);
            }
        }
        if (collision.gameObject.tag == "Miner")
        {
            Debug.Log("P2MinerHit");
            if (powerUpLightningCountdown == true || p1H2powerUpLightningCountdown == true)
            {
                if (H2Lightning.activeInHierarchy == true)
                {
                    H2Lightning.SetActive(false);
                }
                else
                    H2Lightning.SetActive(true);
            }
        }
       
        if (collision.gameObject.tag == "P2Fortune")
        {
            if (powerUpLightningCountdown == true || p2H1powerUpLightningCountdown == true)
            {
                if (H2Lightning.activeInHierarchy == true)
                {
                    H2Lightning.SetActive(false);
                }
                else
                    H2Lightning.SetActive(true);
            }
        }
        if (collision.gameObject.tag == "P2Miner")
        {
            Debug.Log("P2MinerHit");
            if (powerUpLightningCountdown == true || p2H2powerUpLightningCountdown == true)
            {
                if (H2Lightning.activeInHierarchy == true)
                {
                    H2Lightning.SetActive(false);
                }
                else
                    H2Lightning.SetActive(true);
            }
        }
        if (collision.gameObject.tag == "P2BigBoy")
        {
            if (powerUpLightningCountdown == true || p2H3powerUpLightningCountdown == true)
            {
                if (H2Lightning.activeInHierarchy == true)
                {
                    H2Lightning.SetActive(false);
                }
                else
                    H2Lightning.SetActive(true);
            }
        }
    }

    IEnumerator dCooldown()
    {
        dashCooldown = true;
        yield return new WaitForSeconds(dashCooldownTimer);
        dashCooldown = false;

    }


    IEnumerator Lightning()
    {
        audioLightnigh.Play();
        powerUpLightningCountdown = true;
        H2Lightning.SetActive(true);
        yield return new WaitForSeconds(10);
        powerUpLightningCountdown = false;
            if(H2Lightning.activeInHierarchy == true)
            {
                h2lightNightDisable = true;
            }
        H2Lightning.SetActive(false);
        yield return new WaitForSeconds(5);
        h2lightNightDisable = false;
        
    }

    IEnumerator Lightning2()
    {
        yield return new WaitForSeconds(10);

        if (H2Lightning.activeInHierarchy == true)
        {
            h2lightNightDisable = true;
            H2Lightning.SetActive(false);
        }
        yield return new WaitForSeconds(5);
        h2lightNightDisable = false;
    }

    IEnumerator Push()
    {
        pushExclamationMrk.SetActive(true);
        PowerUpPush = true;
        fireTrail.SetActive(true);
        yield return new WaitForSeconds(3);
        fireTrail.SetActive(false);
        PowerUpPush = false;
        pushExclamationMrk.SetActive(false);
    }

    IEnumerator Reverse()
    {
        reverseMove = true;
        confuseMarks.SetActive(true);
        yield return new WaitForSeconds(5);
        confuseMarks.SetActive(false);
        reverseMove = false;
    }
}
