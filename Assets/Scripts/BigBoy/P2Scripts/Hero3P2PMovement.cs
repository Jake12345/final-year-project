﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Hero3P2PMovement : MonoBehaviour {

    public Camera cameraq;
    public float acceleration;
    private float reverseAcceleration;
    private Rigidbody rb;
    private AudioSource rollSound;

    private bool spacePressed = false;
    public static bool gagefull = false;

   
    private GameObject spawnLocation;

    private bool startIntro = true;

    public static bool powerUpLightningCountdown = false;
  
    private Text livesText;
    [SerializeField]
    public static int h2Lives;

    public static Vector3 ballLocation;

    private bool PowerUpPush;

    private bool h2lightNightDisable = false;
   
    [SerializeField]
    private AudioSource audioLightnigh;

    public static bool reverseMove;
    [SerializeField]
    private float PowerReverseTime;
    [SerializeField]
    private GameObject confuseMarks;
    [SerializeField]
    private GameObject fireTrail;
    [SerializeField]
    private GameObject H2Lightning;
    [SerializeField]
    private GameObject pushExclamationMrk;

    private float buffer = 1.5f;
    float x = 0;


    private bool grow = false;
    private bool shrink;

    private bool p1H1powerUpLightningCountdown = false;
    private bool p1H2powerUpLightningCountdown = false;
    private bool p1H3powerUpLightningCountdown = false;
    private bool p2H1powerUpLightningCountdown = false;
    private bool p2H2powerUpLightningCountdown = false;

    public static bool respawning = false;

    private bool dashCooldown = false;
    [SerializeField]
    private int dashCooldownTimer;


    // Use this for initialization
    void Start()
    {
        livesText = GameObject.Find("P2Lives").GetComponent<Text>();
        h2Lives = GameSelect.lives;
        //h2Lives = 4;
        reverseAcceleration = acceleration / 2;
        rb = GetComponent<Rigidbody>();
        rollSound = GetComponent<AudioSource>();
        livesText.text = "Lives: " + h2Lives.ToString();

        spawnLocation = GameObject.Find("Player2Spawn ");
    }



    void Update()
    {

        respawning = false;

        ballLocation = new Vector3(transform.position.x, transform.position.y + buffer, transform.position.z);

        p1H1powerUpLightningCountdown = H1PlayerMovment.powerUpLightningCountdown;
        p1H2powerUpLightningCountdown = Hero2PlayerMovment.powerUpLightningCountdown;
        p1H3powerUpLightningCountdown = Hero3Movment.powerUpLightningCountdown;
        p2H1powerUpLightningCountdown = P2H1Movement.powerUpLightningCountdown;
        p2H2powerUpLightningCountdown = P2H2Movement.powerUpLightningCountdown;
       

        grow = P2H3Ability.grow;
        shrink = P2H3Ability.shrink;

        if (grow == true)
        {
            buffer = buffer + 0.25f * Time.deltaTime;
        }

        if (shrink == true)
        {
            buffer = buffer - 0.25f * Time.deltaTime;
        }

        gagefull = Hero2AbilityGage.gagefull;
        startIntro = StartCountdown.startIntro;

        x = Input.GetAxis("HorizontalP2");

        if (startIntro == false)
        {
            if (h2lightNightDisable == false)
            {
                if (x > 0 || (Input.GetKey(KeyCode.Keypad8)))
                {
                    if (reverseMove == true)
                    {
                        rb.AddForce(acceleration * -cameraq.transform.forward * Time.deltaTime);
                        rb.AddForce(-acceleration * cameraq.transform.up * Time.deltaTime);
                    }
                    else
                    {
                        rb.AddForce(acceleration * cameraq.transform.forward * Time.deltaTime);
                    }
                }

                if (x < 0 || (Input.GetKey(KeyCode.Keypad5)))
                {
                    if (reverseMove == true)
                    {
                        rb.AddForce(acceleration * cameraq.transform.forward * Time.deltaTime);
                    }
                    else
                    {
                        rb.AddForce(acceleration * -cameraq.transform.forward * Time.deltaTime);
                        rb.AddForce(-acceleration * cameraq.transform.up * Time.deltaTime);
                    }
                }

                if (Input.GetKeyUp(KeyCode.Keypad0) && dashCooldown == false || Input.GetKeyDown(KeyCode.Joystick1Button5) && dashCooldown == false)
                {
                    rb.AddForce(cameraq.transform.forward * 4500);
                    StartCoroutine(dCooldown());

                }
            }
        }

        if (PowerUpPush == true)
        {
            rb.AddForce(10000 * cameraq.transform.forward * Time.deltaTime);
            Debug.Log("H2PowerUpPush ACTIVATED!!!");
        }
    }
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "PowerUp")
        {
            int randomNumber = UnityEngine.Random.Range(0, 3);
            //int randomNumber = 0;

            Debug.Log(randomNumber);

            if (randomNumber == 0)
            {
                StartCoroutine(Reverse());
            }
            if (randomNumber == 1)
            {
                StartCoroutine(Push());
            }
            if (randomNumber == 2)
            {
                StartCoroutine(Lightning());
            }
        }
    }
    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Floor")
        {
            respawning = true;
            rb.velocity = Vector3.zero;

            h2Lives--;
            if (1 > h2Lives)
            {
                SceneManager.LoadScene("WinLoseScene");
            }
            Debug.Log("P2H3 respawning");
            this.transform.position = spawnLocation.transform.position;
            cameraq.transform.position = spawnLocation.transform.position;
            livesText.text = "Lives: " + h2Lives.ToString();       
        }

        if (p1H1powerUpLightningCountdown == true || p1H2powerUpLightningCountdown == true || p1H3powerUpLightningCountdown == true || p2H1powerUpLightningCountdown == true || p2H2powerUpLightningCountdown == true)
        {
            StartCoroutine(Lightning2());
        }

        if (collision.gameObject.tag == "Fortune")
        {
            if (powerUpLightningCountdown == true || p1H1powerUpLightningCountdown == true)
            {
                if (H2Lightning.activeInHierarchy == true)
                {
                    H2Lightning.SetActive(false);
                }
                else
                    H2Lightning.SetActive(true);
            }
        }
        if (collision.gameObject.tag == "Miner")
        {
            Debug.Log("P2MinerHit");
            if (powerUpLightningCountdown == true || p1H2powerUpLightningCountdown == true)
            {
                if (H2Lightning.activeInHierarchy == true)
                {
                    H2Lightning.SetActive(false);
                }
                else
                    H2Lightning.SetActive(true);
            }
        }
        if (collision.gameObject.tag == "BigBoy")
        {
            if (powerUpLightningCountdown == true || p1H3powerUpLightningCountdown == true)
            {
                if (H2Lightning.activeInHierarchy == true)
                {
                    H2Lightning.SetActive(false);
                }
                else
                    H2Lightning.SetActive(true);
            }
        }

        if (collision.gameObject.tag == "P2Fortune")
        {
            if (powerUpLightningCountdown == true || p2H1powerUpLightningCountdown == true)
            {
                if (H2Lightning.activeInHierarchy == true)
                {
                    H2Lightning.SetActive(false);
                }
                else
                    H2Lightning.SetActive(true);
            }
        }
        if (collision.gameObject.tag == "P2Miner")
        {
            Debug.Log("P2MinerHit");
            if (powerUpLightningCountdown == true || p2H2powerUpLightningCountdown == true)
            {
                if (H2Lightning.activeInHierarchy == true)
                {
                    H2Lightning.SetActive(false);
                }
                else
                    H2Lightning.SetActive(true);
            }
        }
      
    }

    IEnumerator dCooldown()
    {
        dashCooldown = true;
        yield return new WaitForSeconds(dashCooldownTimer);
        dashCooldown = false;

    }

    IEnumerator Push()
    {
        pushExclamationMrk.SetActive(true);
        PowerUpPush = true;
        fireTrail.SetActive(true);
        yield return new WaitForSeconds(3);
        fireTrail.SetActive(false);
        PowerUpPush = false;
        pushExclamationMrk.SetActive(false);
    }

    IEnumerator Lightning()
    {
        audioLightnigh.Play();
        powerUpLightningCountdown = true;
        H2Lightning.SetActive(true);
        yield return new WaitForSeconds(10);
        powerUpLightningCountdown = false;
        if (H2Lightning.activeInHierarchy == true)
        {
            h2lightNightDisable = true;
        }
        H2Lightning.SetActive(false);
        yield return new WaitForSeconds(5);
        h2lightNightDisable = false;
    }

    IEnumerator Lightning2()
    {
        yield return new WaitForSeconds(10);

        if (H2Lightning.activeInHierarchy == true)
        {
            h2lightNightDisable = true;
            H2Lightning.SetActive(false);
        }
        yield return new WaitForSeconds(5);
        h2lightNightDisable = false;
    }

    IEnumerator Reverse()
    {
        reverseMove = true;
        confuseMarks.SetActive(true);
        yield return new WaitForSeconds(5);
        confuseMarks.SetActive(false);
        reverseMove = false;
    }
}
