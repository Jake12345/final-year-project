﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class P2H3Ability : MonoBehaviour
{
    public float maxHealth;
    public float health;
    private GameObject healthbar;
    Collision collision;

    public static bool gagefull = false;
    private GameObject gageFullBar;
    private bool gageFullSoundPlayed = false;

    public AudioClip[] audioClips;

    [SerializeField]
    private float growShrinkTime;
    [SerializeField]
    private float BigTime;

    [SerializeField]
    public static bool grow = false;
    [SerializeField]
    public static bool shrink = false;

    public static bool increaseSpeed = false;

    [SerializeField]
    private Transform explosionPrefab;
    private bool explosionsActive = false;

    private bool startIntro = true;


    // Use this for initialization
    void Start()
    {
        healthbar = GameObject.Find("P2HealthBar ");
        gageFullBar = GameObject.Find("P2GageFull");
        health = maxHealth;
        gageFullBar.SetActive(false);
        BigTime = BigTime + growShrinkTime;    
    }

    // Update is called once per frame
    void Update()
    {
        startIntro = StartCountdown.startIntro;

        if (health > 0)
        {
            DecreaseHealth();
        }

        if (health == 0)
        {
            if (gageFullSoundPlayed == false)
            {
                PlaySound(0);
                gageFullSoundPlayed = true;
            }
            gagefull = true;
            gageFullBar.SetActive(true);
        }

        if (startIntro == false)
        {
                if (Input.GetKeyUp(KeyCode.Keypad9) && gagefull == true || Input.GetKeyDown(KeyCode.Joystick1Button4) && gagefull == true)
                {
                    gagefull = false;
                    health = maxHealth;
                    gageFullBar.SetActive(false);
                    gageFullSoundPlayed = false;
                    StartCoroutine(getBig());
                    StartCoroutine(getSmall());
                    health = maxHealth;
                    Debug.Log("player2Big");
                }
        }

        if (grow == true)
        {
            this.transform.localScale += new Vector3(0.5f, 0.5f, 0.5f) * Time.deltaTime;
            increaseSpeed = true;
            this.GetComponent<Rigidbody>().mass = this.GetComponent<Rigidbody>().mass + 0.004f;
        }

        if (shrink == true)
        {
            this.transform.localScale -= new Vector3(0.5f, 0.5f, 0.5f) * Time.deltaTime;
            increaseSpeed = false;
            this.GetComponent<Rigidbody>().mass = this.GetComponent<Rigidbody>().mass - 0.004f;
        }
    }

    void PlaySound(int clip)
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.clip = audioClips[clip];
        audio.Play();
    }

    IEnumerator getBig() //after a 02 second delay the mine wil spawn at the location the ball was at 0.2 seconnds 
    {
        grow = true;
        PlaySound(3);
        yield return new WaitForSeconds(growShrinkTime);
        PlaySound(1);
        explosionsActive = true;
        grow = false;
    }
    IEnumerator getSmall() //after a 02 second delay the mine wil spawn at the location the ball was at 0.2 seconnds 
    {
        yield return new WaitForSeconds(BigTime);
        explosionsActive = false;
        shrink = true;
        yield return new WaitForSeconds(growShrinkTime);
        shrink = false;
    }

    void DecreaseHealth()
    {
        health = health - 1;
        float currentHealth = health / maxHealth;
        SetHealthBar(currentHealth);
    }

    public void SetHealthBar(float myHealth)
    {
        healthbar.transform.localScale = new Vector3(Mathf.Clamp(myHealth, 0f, 1f), healthbar.transform.localScale.y, healthbar.transform.localScale.z);//This is what lowers the health bar
    }

    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.layer == 12 && explosionsActive == true)
        {
            Quaternion rot = this.transform.rotation;
            Vector3 pos = this.transform.position;
            Instantiate(explosionPrefab, pos, rot);
            PlaySound(2);
        }
    }
}



  