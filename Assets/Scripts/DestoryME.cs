﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class DestoryME : MonoBehaviour {

    [SerializeField]
    private Renderer rend;

    private Vector3 baselocation;
    private float moveSpawn = 100.0f;
    private bool powerUpPicked = false;
    Vector3 newPos = new Vector3(100.0f, 200.0f, 1000f);
   
    void Start ()
    {
      baselocation = new Vector3(transform.position.x, transform.position.y, transform.position.z);
       
    }
	
	// Update is called once per frame
	void Update ()
    {

    }


    void OnTriggerEnter(Collider other)
    {
        if (powerUpPicked == false)//stop it from looping all the time
        {
            transform.position = newPos;
            StartCoroutine(Respawn());
         
        }
     
    }
    void OnCollisionEnter(Collision collision)
    {

       
    }

 
    IEnumerator Respawn()
    {
        powerUpPicked = true;
        //rend.enabled = false;
        
        //Debug.Log("ObjectDestoryed");
        yield return new WaitForSeconds(10);
        transform.position = baselocation;
        //transform.position = new Vector3(transform.position.x, transform.position.y - 100.0f, transform.position.z);
        powerUpPicked = false;
       // rend.enabled = true;
        //this.gameObject.SetActive(true);
    }
}
