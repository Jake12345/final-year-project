﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ResultsPageScript : MonoBehaviour {




    private int player1Hero;
    private int player2Hero;
    private int p1Lives;
    private int p2Lives;

    [SerializeField]
    private GameObject p1BigBoy;
    [SerializeField]
    private GameObject p1Miner;
    [SerializeField]
    private GameObject p1Fortune;
    [SerializeField]
    private GameObject p2BigBoy;
    [SerializeField]
    private GameObject p2Miner;
    [SerializeField]
    private GameObject p2Fortune;


    [SerializeField]
    private Text p1Result;
    [SerializeField]
    private Text p2Result;

    
 

    // Use this for initialization
    void Start ()
    {

        p1Miner.SetActive(false);
        p1Fortune.SetActive(false);
        p1BigBoy.SetActive(false);
        p2BigBoy.SetActive(false);
        p2Miner.SetActive(false);
        p2Fortune.SetActive(false);

        player1Hero = CharacterSelect.player1Choice;
        player2Hero = CharacterSelect.player2Choice;

        //get the correct life
        switch (player1Hero)
        {
            case 0:
                p1Lives = Hero2PlayerMovment.h2Lives;  
                break;
            case 1:              
                p1Lives = H1PlayerMovment.h2Lives;
                break;
            case 2:
                p1Lives = Hero3Movment.h3Lives;
                break;
        }
        switch (player2Hero)
        {
            case 0:
                p2Lives = P2H1Movement.h2Lives;
                break;
            case 1:
                p2Lives = P2H2Movement.h2Lives;
                break;
            case 2:
                p2Lives = Hero3Movment.h3Lives;
                break;
        }

        Debug.Log("p1lives: "+p1Lives);
        Debug.Log("p2lives: "+p2Lives);

        //spawn the winner/loser in the correct place
        if (p1Lives == 0)
        {
            p1Result.text = "YOU LOSE!";
            p2Result.text = "YOU WIN!";
           
            switch (player1Hero)
            {
                case 0:
                    p1BigBoy.SetActive(true);
                    break;
                case 1:
                    p1Miner.SetActive(true);
                    break;
                case 2:
                    p1Fortune.SetActive(true);
                    break;
            }
            switch (player2Hero)
            {
                case 0:
                    p2BigBoy.SetActive(true);                
                    break;
                case 1:
                    p2Miner.SetActive(true);
                    break;
                case 2:                  
                    p2Fortune.SetActive(true);
                    break;
            }
        }
        else
        {
            p1Result.text = "YOU WIN!";
            p2Result.text = "YOU LOSE!";

            switch (player1Hero)
            {
                 
                case 0:
                    p1BigBoy.SetActive(true); 
                    break;
                case 1:
                    p1Miner.SetActive(true);
                    break;
                case 2:
                    p1Fortune.SetActive(true);
                    break;
            }
            switch (player2Hero)
            {
                case 0:
                    p2BigBoy.SetActive(true);
                    break;
                case 1:
                    p2Miner.SetActive(true); 
                    break;
                case 2:
                    p2Fortune.SetActive(true);
                    break;
            }
        }




    }
	
	// Update is called once per frame
	void Update () {
		
	}


    public void loadLevel(int level)
    {
        SceneManager.LoadScene("MenuScene");
    }

    public void Quit()
    {
        Application.Quit();
    }


}
