﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class CharacterSelect : MonoBehaviour {

    public static int player1Choice;
    public static int player2Choice;


    private int playersChoosen = 1;

    [SerializeField]
    private GameObject[] MyObjects;

    [SerializeField]
    private Text name;

    [SerializeField]
    private Text playerXChoose;

    [SerializeField]
    private Text bioDescrip;

    [SerializeField]
    private Text abilityDescrip;

    private int selector = 1;

    [SerializeField]
    private AudioClip scrollSound;

    AudioSource audio;

    float controllerSelector = 0;

    // Use this for initialization
    void Start ()
    {

        audio = GetComponent<AudioSource>();

        audio.PlayOneShot(scrollSound, 0.7F);
        playerXChoose.text = "Player 1 Choose Hero";
        selector = 1;
        playersChoosen = 0;

        MyObjects[0].SetActive(false);
        MyObjects[1].SetActive(false);
        MyObjects[2].SetActive(false);

    }
	
	// Update is called once per frame
	void Update ()
    {

        if (playersChoosen == 0)
        {
            playerXChoose.text = "Player 1 Choose Hero";
        }
        if (playersChoosen == -1)
        {
            SceneManager.LoadScene("MenuScene");
        }
        controllerSelector = Input.GetAxis("Selector");
        

        if (selector == 3)
        {
            selector = 0;
        }

        if (selector == -1)
        {
            selector = 2;
        }

        if(selector == 0)
        {
            MyObjects[0].SetActive(true);
            MyObjects[1].SetActive(false);
            MyObjects[2].SetActive(false);
           
            name.text = "BigBoy";
            bioDescrip.text = "Hes big and he bad. So nobody should get in his way or he explodes when he is in a tempter";
            abilityDescrip.text = "He increases his ball size by over 200%! At max size when he hits someone they explode and go flying";
        }

        if (selector == 1)
        {
            MyObjects[0].SetActive(false);
            MyObjects[1].SetActive(true);
            MyObjects[2].SetActive(false);

            name.text = "Miner";
            bioDescrip.text = "The miner use to work in the cookie mines but he found he enjoyed the explosions more than free cookies";
            abilityDescrip.text = "He lays mines so you better watch out where you roll!";

        }

        if (selector == 2)
        {
            MyObjects[0].SetActive(false);
            MyObjects[1].SetActive(false);
            MyObjects[2].SetActive(true);

            name.text = "Fortune";
            bioDescrip.text = "'Fortune faviours the bold' is a qoute often heard before he steals some treasure";
            abilityDescrip.text = "He charges up and fires three waves of bullets in a cone spread!";
        }

        //Debug.Log(controllerSelector);

        if (controllerSelector > 0.7)
        {
            selector++;
        }
        if (controllerSelector < -0.7)
        {
            selector--;
        }

        Debug.Log(playersChoosen);

    }
    public void Choose()
    {
        audio.PlayOneShot(scrollSound, 0.7F);

        if (playersChoosen == 1)//player 2 makes a choice
        {
            player2Choice = selector;
            Debug.Log("Player 2 choose" + selector);
            SceneManager.LoadScene("GameSelect");
        }

        if (playersChoosen == 0)//player one makes a choice
        {
            player1Choice = selector;
            playerXChoose.text = "Player 2 Choose Hero";
            Debug.Log("Player 1 choose : " + selector);
            playersChoosen++;
        }

    }

    public void next()
    {
        audio.PlayOneShot(scrollSound, 0.7F);
        selector++;
        Debug.Log("next button pressed");
    }

    public void back()
    {
        audio.PlayOneShot(scrollSound, 0.7F);
        selector--;
        Debug.Log("back button pressed");
    }


    public void redo()
    {
        audio.PlayOneShot(scrollSound, 0.7F);
        playersChoosen--;
        Debug.Log("redo button pressed");
    }
}
