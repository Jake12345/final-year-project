﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameSelect : MonoBehaviour {

    private int playersChoosen = 1;

    [SerializeField]
    private GameObject[] MyObjects;

    [SerializeField]
    private Text mapName;

    [SerializeField]
    private Text Options;

    [SerializeField]
    private Text bio;
    [SerializeField]
    private Text bioDescrip;

    [SerializeField]
    private Text livesText;

    [SerializeField]
    private Text gameInfo;
    [SerializeField]
    private Text gameRule;

    [SerializeField]
    private Text traits;
    [SerializeField]
    private Text traitssDescrip;

    [SerializeField]
    private GameObject controlsDisplay;

    private int selector = 1;

    private int mapChoice;

   

    public static int lives = 0;
  

    float controllerSelector = 0;

    [SerializeField]
    private AudioClip scrollSound;

    AudioSource audio;


    // Use this for initialization
    void Start ()
    {
      
        audio = GetComponent<AudioSource>();
        controlsDisplay.SetActive(false);
        audio.PlayOneShot(scrollSound, 0.7F);
        Options.text = "Choose Map";
        selector = 1;
        playersChoosen = 0;
        livesText.text = " ";
        gameInfo.text = " ";
        gameRule.text = " ";
        
    }
	
	// Update is called once per frame
	void Update ()
    {

        if(lives < 1)
        {
            lives = 1;
        }
       // Debug.Log(selector);
        controllerSelector = Input.GetAxis("Selector");
      
        if (playersChoosen == -1)
        {
            SceneManager.LoadScene("CharacterSelect");
        }

        if (selector == 2)
        {
            selector = 0;
        }

        if (selector == -1)
        {
            selector = 1;
        }


        if (playersChoosen == 0)
        {
            Options.text = "Choose Map";
            livesText.text = " ";
            gameInfo.text = " ";
            gameRule.text = " ";
            lives = 0;


            if (selector == 0)
            {

                MyObjects[0].SetActive(true);
                MyObjects[1].SetActive(false);
                MyObjects[2].SetActive(false);

                mapName.text = "The Village";
                bioDescrip.text = "Home of the elo clan";
                traitssDescrip.text = "A moving bridge";
            }

            if (selector == 1)
            {

                MyObjects[0].SetActive(false);
                MyObjects[1].SetActive(true);
             

                mapName.text = "The Island";
                bioDescrip.text = "An deserted Island in the middle of the Pasfic. Watch out for high tide! The water is shark invested and enting it will mean your death!";
                traitssDescrip.text = "The water level will periodically rise and fall";

            }
        }

        if(playersChoosen == 1)
        {
            MyObjects[0].SetActive(false);
            MyObjects[1].SetActive(false);
            controlsDisplay.SetActive(false);

            bio.text = " ";
            traits.text = " ";
            mapName.text = " ";
            bioDescrip.text = " ";
            traitssDescrip.text = " ";

            //Debug.Log(lives);
            gameRule.text = " ";
            gameInfo.text = " ";
            livesText.text = "Number of Lifes: " + lives.ToString();

        }

        if (playersChoosen == 2)
        {
            
            //Debug.Log(lives);
            livesText.text = " ";
            controlsDisplay.SetActive(true);
            gameRule.text = "Game Rule";
            gameInfo.text = "The goal of the game is to knock the other player off the map. First person to 0 Wins!";
           
        }


        //sDebug.Log(controllerSelector);

        if (controllerSelector > 0.7)
        {
            selector++;
        }
        if (controllerSelector < -0.7)
        {
            selector--;
        }

    }
    public void Choose()
    {
        audio.PlayOneShot(scrollSound, 0.7F);

        if (playersChoosen == 2)
        {        
           
            Debug.Log("Player 2 choose" + selector);
           
            if (mapChoice == 0)
            {
                SceneManager.LoadScene("SceneDemo");
            }
            else
            {
                SceneManager.LoadScene("Map2");
            }
        }

        if (playersChoosen == 0)
        {
            mapChoice = selector;
            Options.text = "Choose number of lives";
            Debug.Log("Map Choice is : " + selector);
        }

        playersChoosen++;
    }

    public void next()
    {
        selector++;
        audio.PlayOneShot(scrollSound, 0.7F);
        if (playersChoosen == 1)
        {
            lives++;
        }
        Debug.Log("next button pressed");
    }

    public void back()
    {
        selector--;
        audio.PlayOneShot(scrollSound, 0.7F);
        if (playersChoosen == 1)
        {
            lives--;
        }
        Debug.Log("back button pressed");
    }

    public void redo()
    {
        playersChoosen--;
        audio.PlayOneShot(scrollSound, 0.7F);
        Debug.Log("redo button pressed");
    }
}
