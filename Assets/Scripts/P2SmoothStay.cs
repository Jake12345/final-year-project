﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class P2SmoothStay : MonoBehaviour {

    [SerializeField]
    private GameObject Guy;
    private Vector3 point;

    private Vector3 ballLocation;

    private float speedMod = 10.0f;
    private float buffer = 1.5f;

    private bool startIntro;
    private float direction = 0.0f;
    private bool reverseMove;


    [SerializeField]
    private int playerNumber;

    // Use this for initialization
    void Start()
    {
        point = Guy.transform.position;//get target's coords
        transform.LookAt(point);//makes the camera look to it
    }

    // Update is called once per frame
    void Update()
    {

        startIntro = StartCountdown.startIntro;

        if (playerNumber == 1)
        {
            reverseMove = P2H1Movement.reverseMove;
            this.transform.position = ballLocation;
        }

        if (playerNumber == 2)
        {
            reverseMove = P2H2Movement.reverseMove;
            this.transform.position = ballLocation;
        }
        if (playerNumber == 3)
        {
            reverseMove = Hero3P2PMovement.reverseMove;
            this.transform.position = ballLocation;
        }

        direction = Input.GetAxis("VerticalP2");

        if (startIntro == false)
        {
            if (reverseMove == true) // this one needs to reverse the direction 
            {
                direction *= -1;

                transform.RotateAround(point, new Vector3(0.0f, direction, 0.0f), 20 * Time.deltaTime * speedMod);

                if (Input.GetKey(KeyCode.Keypad4))
                {
                    transform.RotateAround(point, new Vector3(0.0f, 1.0f, 0.0f), 20 * Time.deltaTime * speedMod);
                }
                if (Input.GetKey(KeyCode.Keypad6))
                {
                    transform.RotateAround(point, new Vector3(0.0f, -1.0f, 0.0f), 20 * Time.deltaTime * speedMod);
                }
            }
            else
            {
                transform.RotateAround(point, new Vector3(0.0f, direction, 0.0f), 20 * Time.deltaTime * speedMod);

                if (Input.GetKey(KeyCode.Keypad4))
                {
                    transform.RotateAround(point, new Vector3(0.0f, -1.0f, 0.0f), 20 * Time.deltaTime * speedMod);
                }
                if (Input.GetKey(KeyCode.Keypad6))
                {
                    transform.RotateAround(point, new Vector3(0.0f, 1.0f, 0.0f), 20 * Time.deltaTime * speedMod);
                }

            }
        }
    }

    void LateUpdate() //last thing done and after all the animations
    {
        if (playerNumber == 1)
        {
            ballLocation = P2H1Movement.ballLocation;
            this.transform.position = ballLocation;
        }

        if (playerNumber == 2)
        {
            ballLocation = P2H2Movement.ballLocation;
            this.transform.position = ballLocation;
        }
        if (playerNumber == 3)
        {
            ballLocation = Hero3P2PMovement.ballLocation;
            this.transform.position = ballLocation;
        }

        // transform.rotation = new Quaternion(0, 0, 0, 0);
    }

}
