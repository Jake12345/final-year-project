﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StartCountdown : MonoBehaviour {

    public Text countdown;

    public static bool startIntro;


	// Use this for initialization
	void Start ()
    {
        startIntro = true;
        StartCoroutine(intro());
    }
	
	// Update is called once per frame
	void Update ()
    {
		
	}


    IEnumerator intro() //after a 02 second delay the mine wil spawn at the location the ball was at 0.2 seconnds 
    {
     
        yield return new WaitForSeconds(3.0f);

        countdown.text = "3";
        yield return new WaitForSeconds(1.0f);

        countdown.text = "2";
        yield return new WaitForSeconds(1.0f);

        countdown.text = "1";
        yield return new WaitForSeconds(1.0f);

        countdown.text = "GO!";
        startIntro = false;
        yield return new WaitForSeconds(2.0f);

        countdown.text = " ";
    }

}
